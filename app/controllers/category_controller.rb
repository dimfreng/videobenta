class CategoryController < ApplicationController

 def getCategory    
    @category = Category.all
    render json: { :status => "success" , :category => @category }, status: :ok    
 end

 def addCategory
    respond_to do |format|
      @category = Category.new(category_params)
      if @category.save
        format.json { render json:{:status => "success",:category => @category}, status: :created }
      else
        format.json { render json:{:status => "unable to save"}, status: :unprocessable_entity}
      end
    end
 end

 def updateCategory
    respond_to do |format|
      
    end
  end

  def deleteCategory
    respond_to do |format|
      
    end
  end


private

 def category_params
  params.require(:category).permit(:name)
 end

end
