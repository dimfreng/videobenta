class VendorController < ApplicationController

 def getVendor    
  if @vendor = Vendor.find_by_facebook_id(params[:vendor][:facebook_id])
    render json:{:status => "success",:vendor => @vendor}, status: :ok
  else
    render json:{:status => "success",:vendor => "vendor not found"}, status: :ok
  end
 end

 def getVendorDetails
  if @vendor = Vendor.find_by_id(params[:vendor][:vendor_id])
    render json: { :status => "success", :vendor => @vendor }
  else
    render json: { :status => "failed" }
  end
 end

 def checkVendor    

      if @vendor = Vendor.find_by_facebook_id(params[:vendor][:facebook_id])

          render json:{ :status => "success" }, status: :created 

      else
        @vendor = Vendor.new(vendor_params)  

          if @vendor.save
            render json:{:status => "success"}, status: :created 
          else
            render json:{:status => "unable to save"}, status: :unprocessable_entity
          end
      end
    
 end

 def updateVendor
    respond_to do |format|
      
    end
  end

  def deleteVendor
    respond_to do |format|
      
    end
  end


  private

 def vendor_params
  params.require(:vendor).permit(:facebook_id, :email, :name, :gender)
 end

end
