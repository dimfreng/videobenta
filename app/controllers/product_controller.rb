class ProductController < ApplicationController

 def getAllProduct
    @product = Product.all.order('created_at DESC')
    render json: { :status => "success" , :category => @product } 
 end

 def getByCategory
  @product = Product.where(:category_id => params[:product][:category_id]).order('created_at DESC')
  render json: { :status => "success" , :category => @product }
 end

 def getByVendor
  @product = Product.where(:vendor_id => params[:product][:vendor_id]).order('created_at DESC')
  render json: { :status => "success" , :category => @product }
 end

 def getByProductName 
  regex = "%#{ params[:product][:name] }%"

  @product = Product.where( "name LIKE ?" , regex ).where(:vendor_id => params[:product][:vendor_id]).order('created_at DESC')
  @products = Product.where( "name LIKE ?" , regex ).order('created_at DESC')
  @all = @product + @products 
  render json: { :status => "success" , :category => @all.uniq }
 end

 def addProduct    
  @product = Product.new(product_params)
    if @product.save
      render json: { :status => "success",:product => @product , :category => @product.category , :owner => @product.vendor } , status: :created 
    else
      render json: { :status => "unable to save" } , status: :unprocessable_entity
    end  
 end

 def updateProduct
    respond_to do |format|
      
    end
  end

  def deleteProduct
    respond_to do |format|
      
    end
  end

  private

  def product_params
    params.require(:product).permit(:name,:vendor_id,:category_id,:price,:link,:description,:contacts)
  end

end
