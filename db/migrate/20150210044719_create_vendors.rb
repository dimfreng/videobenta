class CreateVendors < ActiveRecord::Migration
  
  def up
    create_table :vendors do |t|
      t.column :facebook_id ,:string
      t.column :email ,:string
      t.timestamps
    end
  end

  def down
  	drop_table :vendors
  end
  
end
