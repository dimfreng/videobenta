class AddProduct < ActiveRecord::Migration
  
  def up
  	add_column :products, :name, :string
  	add_column :products, :vendor_id, :string, :null => false, :default => "n/a"
  	add_column :products, :category_id,:string, :null => false, :default => "n/a"    
  	add_column :products, :price, :string
  	add_column :products, :link, :string    
    add_column :products, :description, :string
    add_column :products, :contacts, :string
  	add_index :products, :vendor_id
  	add_index :products, :category_id
  end

  def down
  	remove_index :products,:category_id
  	remove_index :products,:vendor_id
  	remove_column :products, :link
  	remove_column :products, :price
  	remove_column :products, :category_id
  	remove_column :products, :vendor_id
  	remove_column :products, :name    
    remove_column :products, :description
    remove_column :products, :contacts
  end

end
