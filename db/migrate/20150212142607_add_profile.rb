class AddProfile < ActiveRecord::Migration
  
  def up
  	add_column :vendors,:name,:string
  	add_column :vendors,:gender,:string
  end

  def down
  	remove_column :vendors, :gender
  	remove_column :vendors, :name
  end

end
